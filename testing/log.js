const log = (...args) => console.log.call(
  console,
  '--------------\n',
  ...args,
  '\n--------------\n'
);

module.exports = { log };
