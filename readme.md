## Functional programming utils

This is FP project (aka lodash, rambda) to build FP stuff manually for better understanding how it works.

## ToDo

- [x] Install Mocha.
- [ ] Functions
  - [x] `curry`
  - [ ] `bind`
  - [x] `reduce` (with ability iterates by objects)
  - [ ] `compose`
  - [ ] `memoize(fn)`
  - [ ] `memoizeWith(resolver, fn)` `resolver` - resolve cache key (supply additional mutating params)
  - [ ] `find`
  - [ ] `first`
  - [ ] !! `takeFirst(n)`
  - [ ] `map` 
  - [ ] `flatMap`
  - [ ] ? `transduce`
  - [ ] `forEach`
  - [ ] `forIn`
  - [ ] `forOwn`
  
