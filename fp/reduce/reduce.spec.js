const assert = require('assert');

const { reduce } = require('./reduce');

describe('#reduce', () => {
  const arrayReducer = (acc, x) => (acc.push(x), acc);
  const arrayDoubleReducer = (acc, x) => (acc.push(x * 2), acc);
  const sumReducer = (acc, x) => acc + x;
  const mapReducer = (acc, x, key) => (acc[key] = x, acc);

  const arrData = [1, 2, 3];
  const mapData = { a: 1, b: 2, c: 3 };

  const providers = [
    {
      args: { reducer: arrayReducer, seed: [], data: arrData },
      expected: [1, 2, 3]
    },
    {
      args: { reducer: arrayDoubleReducer, seed: [], data: arrData },
      expected: [2, 4, 6]
    },
    {
      args: { reducer: sumReducer, seed: 0, data: arrData },
      expected: 6
    },
    {
      args: { reducer: mapReducer, seed: {}, data: mapData },
      expected: mapData
    },
  ];


  providers.forEach((provider, i) => {
    it(`should returns correct result`, () => {
      const args = provider.args;
      const res = reduce(args.reducer, args.seed, args.data);

      assert.deepEqual(provider.expected, res, `provider index: ${i}, ${provider.args}`);
    })
  });


  it('it should produce arguments to reducer properly', () => {
    const data = [1, 2, 3];

    let isFired = false;
    let _i = 0;

    const reducer = (acc, x, i, arr) => {
      isFired = true;

      assert.equal(acc.length, _i);
      assert.equal(i, _i);
      assert.equal(x, data[_i]);
      assert.equal(arr[_i], data[_i]);

      acc.push(data[i]);
      _i++;

      return acc;
    };

    const res = reduce(reducer, [], data);

    assert.ok(isFired);
    assert.ok(res);
  });

  it('must throw exception when arguments are not valid', () => {
    const invalidArgs = [
      [],
      [ {}, {}, {} ],
      [ null, null, null ],
      [ undefined, [], [1, 2, 3] ],
      [ (acc, x) => acc, [], undefined ],
      [1, 2, 3] // 1 - not a function.
    ];

    invalidArgs.forEach((args, i) => {
      assert.throws(
        () => reduce(...args),
        TypeError,
        `Index: ${i}`
      );
    });

  })

});
