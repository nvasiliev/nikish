/**
 * @example:
 *  const sumReducer = (acc, x) => acc + x;
 *  reduce(sumReducer, [], [1, 2, 3]); // => 6
 *
 *  const arrayReducer = (acc, x) => (acc.push(x * 2), acc);
 *  reduce(sumReducer, [], [1, 2, 3]); // => [2, 4, 6]
 *
 * @param {Function} fn - reducer
 * @param {(Array|Object)} seed - initial value for accumulator
 * @param {(Array|Object)} xs - iterable
 * @returns {*} acc - accumulator after reducing
 */
const reduce = (fn, seed, xs) => {
  failIfInvalidArgs(fn, seed, xs);
  const acc = cloneSeed(seed);
  const reduceFn = resolveReduceFn(xs);

  return reduceFn(fn, acc, xs);
};

module.exports = { reduce };

function failIfInvalidArgs(...args) {
  const isValidArgsLength = args.length === 3;

  // todo: Create `filter`.
  const hasInvalidArgs = args.filter(arg => arg === null || arg === undefined).length > 0;

  const reducer = args[0];
  const isValidReducer = !!reducer && reducer instanceof Function;

  if (
    !isValidArgsLength ||
    hasInvalidArgs ||
    !isValidReducer
  ) {
    throw new TypeError('Not valid arguments');
  }
}

function resolveReduceFn(xs) {
  return xs instanceof Array
    ? reduceArr
    : reduceMap;
}

function reduceArr(fn, acc, xs) {
  // @todo: Create `forEach`.
  xs.forEach((...args) => {
    acc = fn(acc, ...args);
  });

  return acc;
}

function reduceMap(fn, acc, xs) {
  // @todo: Create `forOwn`.
  for (const key in xs) {
    acc = fn(acc, xs[key], key, xs);
  }

  return acc;
}

// @todo: check for the `Map`, Object, etc.
// @todo: Create `assign`.
function cloneSeed(seed) {
  return (
    seed instanceof Array ? seed.slice() :
    seed instanceof Object ? {...seed} :
    seed
  );
}
