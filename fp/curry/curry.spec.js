const assert = require('assert');

const { curry } = require('./curry');

describe('#curry', () => {

  let sum;

  beforeEach(() => {
    sum = curry((a, b, c) => a + b +c);
  });

  it('should execute the curried fn if arguments is enough', () => {
    const res = sum(1, 2, 3);

    assert.equal(6, res);
  });

  it('should execute curried fn when gets last arg', () => {
    const sum = curry((a, b, c) => a + b +c);
    const res = sum(1)(2)(3);

    assert.equal(6, res);
  });

  it('should return the wrapper fn when the arguments is not enough', () => {
    let res = sum(1);
    assert.ok(res instanceof Function);

    res = res(2);
    assert.ok(res instanceof Function);

    assert.equal(6, res(3));
  });

  it('should given result if given args count is not consistent, e.g (a, b)(c)', () => {
    assert.equal(6, sum(1, 2)(3));
  });

  it('should fails if given args is more then then fn args', () => {
    assert.throws(() => sum(1)(2)(3)(4));
  })

});
