/**
 * @example
 * const abc = (a, b, c) => [a, b, c];
 * curried = curry(abc);
 *
 * curried(1)(2)(3); // => [1, 2, 3]
 * curried(1, 2)(3); // => [1, 2, 3]
 * curried(1)(2)(3) // => [1, 2, 3]
 *
 * @param {Function} fn
 * @returns {function(...[*])}
 */
const curry = (fn) => {
  const wrapper = (...args) => (
    args.length === fn.length
      ? fn.apply(undefined, args)
      : wrapper.bind(undefined, ...args)
  );

  return wrapper;
};

module.exports = { curry };
