### Project notes

```
const compose = (...fns) =>
  fns.reduceRight((acc, fn) => (...args) => fn(acc(...args)), x => x);
```

```
const compositionOf = (acc, fn) => (...args) => fn(acc(...args));

// Теперь можно написать функцию compose для получения композиции произвольного количества функций как редукции её аргументов:

const compose = (...fns) =>
  reduce(compositionOf, x => x, fns);
```

```
// left-to-righe flow
const pipe = (...fns) => 
  x => 
    fns.reduce((v, f) => f(v), x);  
```

memoizing with deps: http://inlehmansterms.net/2015/03/01/javascript-memoization/
